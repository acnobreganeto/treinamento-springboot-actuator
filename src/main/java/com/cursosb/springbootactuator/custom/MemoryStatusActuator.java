package com.cursosb.springbootactuator.custom;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@Endpoint(id = "memorystatus")
public class MemoryStatusActuator {
	@Bean
	@ReadOperation
	public Map<String, Object> memoryStatus() {
		Map<String, Object> mem = new LinkedHashMap<>();
		mem.put("JVM Total memory", Runtime.getRuntime().totalMemory());
		mem.put("JVM Max memory", Runtime.getRuntime().maxMemory());
		mem.put("JVM Free memory", Runtime.getRuntime().freeMemory());

		return mem;
	}
}
